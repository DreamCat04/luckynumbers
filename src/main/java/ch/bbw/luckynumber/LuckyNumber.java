package ch.bbw.luckynumber;

import java.util.*;

/**
 * LuckyNumber
 * Fachklasse für das Berechnen von Glueckszahlen
 *
 * @author Peter Rutschmann
 * @date 06.09.2021
 */
public class LuckyNumber {

	public int singleDice(){
		Random rand = new Random();
		int value = rand.nextInt(7);

		return value;
	}

	public List<Integer> doubleDice(){
		List<Integer> list = new ArrayList<>();
		Random rand = new Random();
		list.add(rand.nextInt(7));
		list.add(rand.nextInt(7));
		return list;
	}

	public boolean trueOrFalse(){
		Random rand = new Random();
		Boolean boo = rand.nextBoolean();
		return boo;
	}

	public Set<Integer> sixLottoNumbers(){
		Set<Integer> set = new HashSet<Integer>();
		//Hier ergaenzen

		return set;
	}

	public String playingCard(){
		//Hier ergaenzen

		return "keine Karte";
	}

	public List<Integer> primeUpTo100(){
		List<Integer> list = new ArrayList<>();
		for (int i = 2; i <= 100; i++){
			if(checkPrime(i)){
				list.add(i);
			}
		}

		return list;
	}

	public int primeNextTo(int value){
	return 0;
	}

	public List<Integer> triangleNumbersUpTo(int value){
		List<Integer> list = new ArrayList<Integer>();
		//Hier ergaenzen

		return list;
	}
	public boolean checkPrime(int number){
		int counter;
		for(counter = 3; number % counter != 0; counter += 2) {
		}

		boolean isPrime;
		if (counter < number) {
			isPrime = false;
		} else {
			isPrime = true;
		}

		return isPrime;
	}
}
